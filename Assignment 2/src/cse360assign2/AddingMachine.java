/*
 * Author:			Luis D. Luvia
 * Class ID: 		R T 6
 * Assignment No.:	2
 */

/**
 * AddingMachine is a Java class designed to perform basic calculations,
 * such as addition, subtraction, multiplication, and division
 * of integers. It can also return a history of calculator
 * operations; internally, every time a new operation is
 * performed, the history of the calculator operations
 * is updated.
 *
 * @author 		Luis D. Luvia
 * @version 	1.0
 * @since		1.0
 */

package cse360assign2;

import java.util.ArrayList;

public class AddingMachine {

	private int total;
	private ArrayList<int[]> historyPairs = new ArrayList<int[]>();

	private enum Operation {
		ADD, SUBTRACT;
	}

	/**
	 * The public method AddingMachine is the constructor for the class.
	 * It sets the private variables to their intended starting values.
	 *
	 * @since		1.0
	 */
	public AddingMachine () {
		total = 0;  // not needed - included for clarity
	}

	/**
	 * Returns the private integer variable <code>total</code> upon request
	 * from another method within the class.
	 *
	 * @return		the private integer <code>total</code>.
	 * @since		1.0
	 */
	public int getTotal () {
		return total;
	}

	/**
	 * Adds the given integer to the <code>total</code> variable and calls
	 * updateHistory to add the addition
	 * operation and the integer parameter to the <code>history</code>
	 * variable.
	 *
	 * @param value	the value to be added to the <code>total</code>
	 *
	 * @since 		1.0
	 */
	public void add (int value) {
		total += value;
		updateHistory(Operation.ADD, value);
	}

	/**
	 * Subtracts the given integer from the <code>total</code>
	 * variable and calls updateHistory to add the subtraction
	 * operation and the integer parameter to the <code>history</code>
	 * variable.
	 *
	 * @param value	the value to be subtracted from <code>total</code>
	 *
	 * @since		1.0
	 */
	public void subtract (int value) {
		total -= value;
		updateHistory(Operation.SUBTRACT, value);
	}

	/**
	 * Formats the total history of operations as a string to be returned for
	 * the calling class to use.
	 *
	 * @since		1.0
	 */
	public String toString () {
		String historyString = new String("0");

		for (int[] pair: historyPairs) {
			switch (pair[0]) {
				case(0):		// 0 is the ADD operation.
					historyString += " + ";
					break;
				case(1):		// 1 is the SUBTRACT operation.
					historyString += " - ";
			}

			historyString += pair[1];
		}

		return historyString;
	}

	/**
	 * After every operation, this method is called to add the operation
	 * and its operand to the private string variable <code>history</code>.
	 *
	 * @param operation	the operation performed on the two operands.
	 * @param operand 	the second operand that was used in the operation.
	 * @since		1.1
	 */
	private void updateHistory(Operation operation, int operand) {
		historyPairs.add(new int[]{operation.ordinal(), operand});
	}

	/**
	 * Resets the history and total to its original state.
	 *
	 * @since		1.0
	 */
	public void clear() {
		total = 0;
		historyPairs.clear();
	}
}
